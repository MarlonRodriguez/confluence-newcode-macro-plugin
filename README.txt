-----------------------------------
| Confluence Newcode Macro Plugin |
-----------------------------------

This is the Newcode Macro plugin. A plugin developed to serve as a replacement 
for the Code plugin delivered by Atlassian in the default installation of 
Confluence. This plugin has to following goals:
* Support the widely used programming languages, like Java, C#, C++ and PHP
* Provide a better-looking result than the Code Macro
* Leave the development of the source code highlighting to someone else, just
  provide the integration code to speed-up development.

Requirements
------------

The Newcode Macro plugin requires an installation of Confluence 2.10 or 
higher. The Java code itself requires JRE 1.5+.

Installation
------------

* Disable the original Code macro.

- This plugin serves as a replacement and uses the same macro key.
- Since 1.9.1 you can configure which macro key you want to use: \{code\} or \{newcode\}. The distributed jar defaults to \{code\}, thus serving as a replacement.

* Install the distribution Jar-file using the Confluence Plugin Manager.

Configuration
-------------

If you want to use {newcode} as the macro key, disable and enable the appropiate plugins in the plugin administration.

As yet there are no other global configuration options which can be set.

Usage
-----

|| Parameter || Since || Required || Description ||
| *language* | 1.0 | no | (default param) the language to highlight, see below for support |
| *title* | 1.9 | no | Set a title for the code |
| *collapse* | 1.0 | no | "true" will collapse the code fragment by default |
| *linenumbers* | 1.0 | no | "true" will show line numbers in the left gutter (up to 1.9.2, default: true) |
| *firstline* | 1.0 | no | if showing line numbers, where to start numbering from (default 1) |
| *controls* | 1.0 | no | "true" will show controls at the top of the code fragment to copy and print (up to 1.9.2, default: true) |
| *ruler* | 1.9 | no | "true" will show a ruler to indicate the columns (default: false) |
| *theme* | 1.9 | no | If set, use the specified theme instead of the default |
| *exportImage | 1.9.3 | no | If set, force export to image instead of text (if possible) |
| _macro body_ | 1.0 | yes | the code to format |

A complete explanation can be found in the notation guide in the "Advanced Formatting" section.

Deinstallation
--------------

* Deinstall the plugin using the Confluence Plugin Manager
* Reinstall or re-enable the original Code macro

Add support for custom languages
--------------------------------

The implementation of the Newcode macro is based on the 2.0 version of the
JavaScript based SyntaxHighlighter by Alex Gorbatchev 
(http://alexgorbatchev.com/wiki/). To add support for new languages, follow
the steps below:

* Create the JavaScript brush file using the description on 
  http://alexgorbatchev.com/wiki/SyntaxHighlighter:Brushes:Custom.
* Store the brush in the appropriate location
  - Jar version: the "sh/scripts" directory within the JAR file
  - Development/SVN version: the "src/main/resources/sh/scripts" directory
* Register the brush in the atlassian-plugin.xml. 
  - This file is located in the root of the JAR file or in the 
    "src/main/resources" directory of the development/svn version.
  - Within this XML file there is a list of default brushes and below that
    comments indicate where you can add your custom brushes. A brush entry
    always has the following format: 
<resource type="download" name="[brush filename]" location="[relative path]"/>
* Register the brush in the languages.xml
** This filed is located in the root of the JAR file or in the "src/main/resources" directory of the development/svn version.
** Within this XML file there is a list of default brushes and aliases. A brush entry always has the following format: 
<builtin-language>
    <name>\[UniqueName\]</name>
    <brushSuffix>\[Suffix of the brush file stored in sh/scripts\]</brushSuffix>
    <aliases>
        <string>\[Alias 1\]</string>
        <string>\[Alias 2\]</string>
        ...
    </aliases>
</builtin-language>
* Submit a bug report within the Newcode JIRA, with type enhancement and attach
  the new brush file. (http://developer.atlassian.com/jira/browse/NCODE)
  - This way your new brush can be added to the list of supported languages
* Submit the brush to the original author to get the brush included in the
  original distribution.
  
History
-------

Development of the Newcode plugin was started by Agnes Ro and Mike Cannon-
Brookes. This plugin (up to version 1.03) supported Confluence v2.2 and up.
Although it provided some basic functionality, there were editing issues and 
the Macro was not on-par with the original Code plugin. Further development
wasn't done due to time constraints.

In February 2009, redevelopment was started by Jeroen Benckhuijsen. This new
plugin was development from the ground up - though heavily inspired by the old
code. The main focus points of this initial development were:
* Solve the outstanding bugs within the current code
* Add support for a lot more languages
* Upgrade to a new version of the SyntaxHighlighter library
* Support export options wihtin Confluence (e.g. export to PDF)
* Migrate the project to the latest Maven 2 infrastructure
* Integrate the plugin within the Confluence Plugin Repository
* Target the newest release of Confluence and use it's features
* Migrate to the V2 renderer framework
* Provide a complete set of test-cases (both unit and integration test) as 
  suggested on the Atlassian Development Network.

Todo
----

The following improvements are scheduled to be added to the Newcode macro
* Add support for Confluence 2.8+

Authors
-------

Up to version 1.03
* Agnes Ro
* Mike Cannon-Brookes

As of version 1.9
* Jeroen Benckhuijsen (Project lead)
