/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.config;

import java.io.Serializable;
import java.io.StringReader;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.render.ContentFormatter;
import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.confluence.setup.settings.SettingsManager;

/**
 * The settings manager is responsible for the persistency and management of the
 * settings of the new code macro.
 * 
 * @author Jeroen Benckhuijsen
 * 
 */
public class NewcodeSettingsManager implements InitializingBean {

    private NewcodeSettings currentSettings = new NewcodeSettings();

    // Injected
    private final SettingsManager settingsManager;

    /**
     * Default constructor.
     */
    public NewcodeSettingsManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;
    }

    /**
     * Returns the current settings. If updated, {@link #updateSettings()} must
     * be called to persist the changes.
     * 
     * @return The current settings
     */
    public NewcodeSettings getCurrentSettings() {
        return currentSettings;
    }

    /**
     * Update the persistent settings.
     */
    public void updateSettings() {
        Map<String, Object> settings = this.currentSettings.settingsToMap();
        this.settingsManager.updatePluginSettings(Constants.PLUGIN_KEY,
                (Serializable) settings);
    }
    
    /**
     * Update the default theme.
     * 
     * @param theme
     *            The new theme
     */
    public void updateDefaultTheme(final String theme) {
        getCurrentSettings().setDefaultTheme(theme);
    }
    
    /**
     * Update the default language.
     * 
     * @param language
     *            The new language
     */
    public void updateDefaultLanguage(final String language) {
        getCurrentSettings().setDefaultLanguage(language);
    }

    /**
     * {@inheritDoc} 
     */
    @SuppressWarnings("unchecked")
    public void afterPropertiesSet() throws Exception {
        Serializable settings = this.settingsManager
                .getPluginSettings(Constants.PLUGIN_KEY);
        if (settings != null && settings instanceof Map) {
            this.currentSettings.mapToSettings((Map<String, Object>) settings);
        } else {
            this.currentSettings = new NewcodeSettings();
        }
    }
}
