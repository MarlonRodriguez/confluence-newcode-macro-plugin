/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.ext.code.config.NewcodeSettings;
import com.atlassian.confluence.ext.code.config.NewcodeSettingsManager;
import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.atlassian.confluence.ext.code.themes.ThemeRegistry;
import com.atlassian.confluence.ext.code.themes.UnknownThemeException;
import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.user.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * This class implements the rendering logic to format the content of the macro.
 * 
 * @author Jeroen Benckhuijsen
 */
public final class ContentFormatter {

    private static final Log LOG = LogFactory.getLog(ContentFormatter.class);

    private static final String PLUGIN_KEY = Constants.PLUGIN_KEY;

    private static final String[] DEFAULT_RESOURCES = {
            "confluence.web.resources:jquery", 
            "confluence.web.resources:ajs",
            PLUGIN_KEY + ":syntaxhighlighter"};

    private static final String[] SH_MARKUP = {
            "<script type=\"syntaxhighlighter\" class=\"", "\"><![CDATA[",
            "]]></script>" };

    private static final String[] WYSIWYG_SH_MARKUP = {
            "<pre class=\"", "\" style=\"font-size:12px;\">",
            "</pre>" };

    private static final int SH_START = 0;
    private static final int SH_MIDDLE = 1;
    private static final int SH_END = 2;

    private final WebResourceManager webResourceManager;
    private final LanguageRegistry languageRegistry;
    private final ThemeRegistry themeRegistry;
    private final LocaleManager localeManager;
    private final DescriptorFacade descriptorFacade;
    private final SettingsManager settingsManager;
    private final NewcodeSettingsManager newcodeSettingsManager;

    private ParameterMapper parameterMapper = new ParameterMapper();

    /**
     * Default constructor.
     */
    public ContentFormatter(SettingsManager settingsManager, DescriptorFacade descriptorFacade, LocaleManager localeManager,
                            ThemeRegistry themeRegistry, LanguageRegistry languageRegistry, WebResourceManager webResourceManager,
                            NewcodeSettingsManager newcodeSettingsManager)
    {
        this.settingsManager = settingsManager;
        this.descriptorFacade = descriptorFacade;
        this.localeManager = localeManager;
        this.themeRegistry = themeRegistry;
        this.languageRegistry = languageRegistry;
        this.webResourceManager = webResourceManager;
        this.newcodeSettingsManager = newcodeSettingsManager;
    }

    /**
     * Format the contents inside the macro based on the parameters. This will
     * add a &lt;script&gt; tag with a specific class and type which will be
     * picked up by the JavaScript based rendering.
     * 
     * @param conversionContext
     *            The rendering context.
     * @param parameters
     *            The parameters specified by the user.
     * @param body
     *            The contents of the macro.
     * @return The formatted content.
     * @throws InvalidValueException
     *             In case of parameter errors
     */
    public String formatContent(final ConversionContext conversionContext, final Map<String, String> parameters, final String body) 
    throws InvalidValueException
    {
        boolean isExport = RenderContextOutputType.PDF.equals(conversionContext.getOutputType())
                || RenderContextOutputType.WORD.equals(conversionContext.getOutputType())
                || RenderContextOutputType.HTML_EXPORT.equals(conversionContext.getOutputType());
        boolean isEmail = RenderContextOutputType.EMAIL.equals(conversionContext.getOutputType())
        		|| RenderContextOutputType.FEED.equals(conversionContext.getOutputType());
        boolean isMobile = "mobile".equals(conversionContext.getOutputDeviceType());

        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting rendering of content");
        }

        /*
         * Map and verify parameters
         */
        Map<String, String> parametersWithDefaults = updateParametersWithDefaults(parameters);
        Map<String, String> mappedParameters = mapParameters(parametersWithDefaults,
                isExport);
        String theme = parameterMapper.getTheme(parametersWithDefaults).toLowerCase();
        String lang = this.parameterMapper.getLanguage(parametersWithDefaults);
        verifyWithRegistry(lang, theme);

        /*
         * Perform rendering of the content
         */
        String[] markup = (isEmail || isExport || isMobile ? WYSIWYG_SH_MARKUP : SH_MARKUP);
        String renderedContent = createRenderContent(body, mappedParameters,
                markup, isExport);

        if (LOG.isDebugEnabled()) {
            LOG.debug("Add web resources needed for rendering");
        }

        if(!isMobile)
        {
             /*
             * Adds the CSS and JavaScript web resources needed for rendering.
             */
            requireNormalWebResources(lang, theme);
        }
        else
        {
            requireMobileResources();
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Rendering of content finished");
        }

        return renderedContent;
    }

    private void requireNormalWebResources(String lang, String theme)  throws InvalidValueException

    {
        for (String resource : DEFAULT_RESOURCES) {
            webResourceManager.requireResource(resource);
        }
        // Include resource for the language
        try {
            webResourceManager.requireResource(languageRegistry.getWebResourceForLanguage(lang));
        } catch (UnknownLanguageException e) {
            throw new InvalidValueException("lang");
        }
        try {
            webResourceManager.requireResource(themeRegistry.getWebResourceForTheme(theme));
        } catch (UnknownThemeException e) {
            throw new InvalidValueException("theme");
        }
        webResourceManager.requireResource(PLUGIN_KEY + getJsLocalizedResource());
    }

    private void requireMobileResources()
    {
        webResourceManager.requireResourcesForContext("confluence.macros.newcode.macro.mobile");
    }
    
    /**
     * Merge the defined defaults for the parameters with the supplied parameters map. The specified parameters
     * take precedence over the defaults.
     * @param parameters The specified parameters.
     * @return The merged parameters
     */
    private Map<String, String> updateParametersWithDefaults(final Map<String, String> parameters) {
        Map<String, String> merged = new HashMap<String, String>(parameters);

        //NCODE-90
        if (merged.containsKey(Parameters.PARAM_LANGUAGE)) {
        	merged.put(Parameters.PARAM_LANG, merged.get(Parameters.PARAM_LANGUAGE)); 
        }

        // Default language fallbacks happen here.
        // - If the macro simply specified no language, fall back to the configured default.
        // - If the macro specifies a language that doesn't exist, fall back to the configured default language. (this may happen if a custom language is un-installed)
        //  - If the specified language is not registered, but it's set to "none", then *DON'T* fall back to the default.
        // (Later on, if the configured default language is not set to anything, then it will fall back to the hard-coded default)
        NewcodeSettings currentSettings = newcodeSettingsManager.getCurrentSettings();
        if (!merged.containsKey(Parameters.PARAM_LANG))
        {
            merged.put(Parameters.PARAM_LANG, currentSettings.getDefaultLanguage());
        }
        else
        {
            String language = merged.get(Parameters.PARAM_LANG).toLowerCase();
            if (!languageRegistry.isLanguageRegistered(language) && !merged.get(Parameters.PARAM_LANG).equals("none"))
            {
                merged.put(Parameters.PARAM_LANG, currentSettings.getDefaultLanguage());
            }
        }

        if (!merged.containsKey(Parameters.PARAM_THEME))
        {
            merged.put(Parameters.PARAM_THEME, currentSettings.getDefaultTheme());
        }

        return merged;
    }
    
    /**
     * Update the parameters with the default layout values used by the theme 
     * @param parameters The parameters
     * @return The updated parameters
     * @throws InvalidValueException In case of parameter errors
     */
    public Map<String, String> getPanelParametersWithThemeLayout(final Map<String, String> parameters) throws InvalidValueException {
        Map<String, String> merged = new HashMap<String, String>(parameters);
        
        Map<String, String> parametersWithDefaults = updateParametersWithDefaults(parameters);
        String theme = parameterMapper.getTheme(parametersWithDefaults).toLowerCase();
        
        Map<String, String> layout;
        try {
            layout = this.themeRegistry.getThemeLookAndFeel(theme);
        } catch (UnknownThemeException e) {
            // Should not occur, just checked existence of the theme
            throw new IllegalStateException("Invalid theme", e);
        }
        for (Map.Entry<String, String> entry : layout.entrySet()) {
            if (!merged.containsKey(entry.getKey()) && entry.getValue() != null) {
                merged.put(entry.getKey(), entry.getValue());
            }
        }
        
        return merged;
    }

    /**
     * Performs mapping to the parameters understood by the syntax highlighter
     * JavaScript and checks the values of the parameters.
     * 
     * @param parameters
     *            The input parameters
     * @param isExport
     *            Whether the user has requested an export
     * @return The mapped parameters
     * @throws InvalidValueException
     *             In case of invalid parameter values
     */
    private Map<String, String> mapParameters(
            final Map<String, String> parameters, final boolean isExport)
            throws InvalidValueException {
        Map<String, String> tmpParameters = new HashMap<String, String>(
                parameters);
        /*
         * Handle export as a parameter
         */
        tmpParameters.put(Parameters.PARAM_EXPORT, Boolean.toString(isExport));
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("Perform mapping of parameters");
        }

        /*
         * Map the parameters
         */
        Map<String, String> mappedParameters = parameterMapper
                .mapParameters(tmpParameters);

        return mappedParameters;
    }
    
    /**
     * Verify the language and theme with the registries.
     * @param lang The specified language.
     * @param theme The specified theme.
     * @throws InvalidValueException In case of an invalid language or theme
     */
    private void verifyWithRegistry(final String lang, final String theme) throws InvalidValueException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Check availability of language and theme");
        }

        if (!this.languageRegistry.isLanguageRegistered(lang)) {
            throw new InvalidValueException("lang");
        }
        
        if (!this.themeRegistry.isThemeRegistered(theme)) {
            throw new InvalidValueException("theme");
        }
    }

    /**
     * Returns the web-resource to include for the localization of the
     * JavaScript messages.
     * @return The localization web resource
     */
    private String getJsLocalizedResource() {
        User user = AuthenticatedUserThreadLocal.getUser();
        Locale locale = this.localeManager.getLocale(user);
        String userLanguage = locale.getLanguage().toLowerCase();
        List<String> localizationList = this.descriptorFacade.listLocalization();
        if (!localizationList.isEmpty() && localizationList.contains(userLanguage))
        {
            return ":syntaxhighlighter-lang-" + userLanguage;
        }
        else
        {
            String globalDefaultLocale = settingsManager.getGlobalSettings().getGlobalDefaultLocale().substring(0, 2).toLowerCase();
            if (localizationList.contains(globalDefaultLocale))
            {
                return ":syntaxhighlighter-lang-" + globalDefaultLocale;
            }
        }
        return ":syntaxhighlighter-lang-en";
    }

    /**
     * Embeds the content in a &lt;pre&gt; tag which is required by the
     * SyntaxHighlighter library to recognize the content to render and adds the
     * correct parameters to this tag based on the macro parameters.
     * 
     * @param body
     *            The content
     * @param parameters
     *            The macro parameters
     * @param markup
     *            The syntax highlighter markup to use
     * @return The embedded content
     */
    private String createRenderContent(final String body,
            final Map<String, String> parameters, final String[] markup,
            boolean isExport) {
        StringBuffer buffer = new StringBuffer();

        buffer.append(markup[SH_START]);

        boolean first = true;
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            if (!first) {
                buffer.append("; ");
            }
            buffer.append(entry.getKey()).append(": ").append(entry.getValue());
            first = false;
        }
        
        buffer.append(markup[SH_MIDDLE]);
    	buffer.append(body);
        buffer.append(markup[SH_END]);

        return buffer.toString();
    }
}
