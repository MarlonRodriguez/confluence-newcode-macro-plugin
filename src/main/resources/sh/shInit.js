AJS.toInit( function() {
	try {
		if (window.SyntaxHighlighter && window.SyntaxHighlighter.config) {
			var contextRoot = (document.getElementById("confluence-context-path").content || "");
			var clipboardUrl =  contextRoot + '/s/${project.version}/_/download/resources/${atlassian.plugin.key}:clipboard/clipboard.swf';
		    window.SyntaxHighlighter.config.clipboardSwf = clipboardUrl;
		}
		window.SyntaxHighlighter.highlight();
	} catch (err) {
		AJS.log(err);
	}
});
