package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.LanguageParser;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

/**
 * Unit tests for {@link RhinoLanguageParser}
 */
public class LanguageParserTestCase extends TestCase
{
    private LanguageParser parser;

    @Override
    protected void setUp() throws Exception
    {
        parser = new RhinoLanguageParser();
    }

    /**
     * Tests that the parser fails correctly when the input is not even proper JavaScript.
     */
    public void testWhenInputIsNotJavaScript() throws Exception
    {
        Reader dummy = new StringReader("WAT? Y U NO INPUT JAVASCRIPT?");
        try
        {
            parser.parseBuiltInLanguage(dummy);
            fail("JavaScript parse error expected.");
        }
        catch (InvalidLanguageException e)
        {
            // expected.
        }
    }

    /**
     * Tests that the parser fails correctly when the input does not register a name for the brush.
     */
    public void testWhenNoBrushNameIsSpecified() throws Exception
    {
        Reader testLanguage = getTestLanguage("testBrush-NoName.js");
        try
        {
            parser.parseBuiltInLanguage(testLanguage);
            fail("JavaScript parse error expected.");
        }
        catch (InvalidLanguageException e)
        {
            assertEquals("newcode.language.parse.no.brush.name", e.getErrorMsgKey());
        }
    }

    /**
     * Tests that the parser fails correctly when the input tries to register aliases that are not Strings.
     */
    public void testWhenAliasesAreInvalid() throws Exception
    {
        Reader testLanguage = getTestLanguage("testBrush-InvalidAliases.js");
        try
        {
            parser.parseBuiltInLanguage(testLanguage);
            fail("Parse error expected.");
        }
        catch(InvalidLanguageException e)
        {
            assertEquals("newcode.language.parse.invalid.alias.type", e.getErrorMsgKey());
        }
    }

    /**
     * Tests that the parser fails correctly when the input does not register any aliases.
     */
    public void testWhenNoAliasesAreSpecified() throws Exception
    {
        Reader testLanguage = getTestLanguage("testBrush-NoAliases.js");
        try
        {
            parser.parseBuiltInLanguage(testLanguage);
            fail("Parse error expected.");
        }
        catch(InvalidLanguageException e)
        {
            assertEquals("newcode.language.parse.no.brush.name", e.getErrorMsgKey());
        }
    }

    /**
     * Tests that the parser fails correctly when the input cannot be read.
     */
    public void testWhenInputReaderIsBorked() throws Exception
    {
        Reader testLanguage = new Reader()
        {
            @Override
            public int read(char[] cbuf, int off, int len) throws IOException
            {
                throw new IOException("uh-oh, it didn't work!");
            }

            @Override
            public void close() throws IOException
            {
                throw new IOException("uh-oh, it didn't work!");
            }
        };

        try
        {
            parser.parseBuiltInLanguage(testLanguage);
            fail("Parse error expected.");

        } catch (InvalidLanguageException e)
        {
            assertEquals("newcode.language.parse.read.failed", e.getErrorMsgKey());
            assertTrue(e.getCause() instanceof IOException);
        }
    }

    /**
     * Tests that parsing a registered language works as expected.
     */
    public void testParseRegisteredLanguage() throws Exception
    {
        Reader testLanguage = getTestLanguage("shBrushBat.js");
        RegisteredLanguage language = parser.parseRegisteredLanguage(testLanguage, "Batch Script");

        assertEquals("Cmd", language.getName());
        assertEquals("Batch Script", language.getFriendlyName());
        assertEquals(3, language.getAliases().size());
    }

    /**
     * Tests that parsing a built-in language works as expected.
     */
    public void testParseBuiltInLanguage() throws Exception
    {
        InputStream is = null;
        try
        {
            is = this.getClass().getClassLoader().getResourceAsStream("sh/scripts/shBrushJava.js");

            BuiltinLanguage language = parser.parseBuiltInLanguage(new InputStreamReader(is));

            assertEquals("Java", language.getName());
            assertEquals(1, language.getAliases().size());
            assertEquals("java", language.getAliases().iterator().next());
        }
        finally
        {
            if (is != null)
                is.close();
        }
    }

    private Reader getTestLanguage(String scriptName) throws IOException
    {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("scripts/" + scriptName);
        String s = IOUtils.toString(resourceAsStream);
        IOUtils.closeQuietly(resourceAsStream);

        return new StringReader(s);
    }
}
