/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.themes.impl;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.descriptor.ThemeDefinition;
import com.atlassian.confluence.ext.code.util.Constants;

/**
 * Provides mocking of the descriptor facade to load the built-in themes
 * for testing purposes.
 * @author Jeroen Benckhuijsen
 *
 */
public final class ThemeDescriptorFacadeMock {
    
    private static final String WEB_RESOURCE_PREFIX = Constants.PLUGIN_KEY + ":sh-theme-";
    
    private static final Map<String, String> defaultConfluenceLayout = new HashMap<String, String>();

    private static final ThemeDefinition[] BUILTIN_THEMES = {
        new ThemeDefinition("sh/styles/shThemeDefault.css", WEB_RESOURCE_PREFIX + "default", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeDjango.css", WEB_RESOURCE_PREFIX + "django", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeEmacs.css", WEB_RESOURCE_PREFIX + "emacs", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeFadeToGrey.css", WEB_RESOURCE_PREFIX + "fadetogrey", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeMidnight.css", WEB_RESOURCE_PREFIX + "midnight", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeRDark.css", WEB_RESOURCE_PREFIX + "rdark", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeEclipse.css", WEB_RESOURCE_PREFIX + "eclipse", new HashMap<String, String>()),
        new ThemeDefinition("sh/styles/shThemeConfluence.css", WEB_RESOURCE_PREFIX + "confluence", defaultConfluenceLayout)
    };
    
    static {
        defaultConfluenceLayout.put("borderColor", "grey");
        defaultConfluenceLayout.put("borderStyle", "dashed");
        defaultConfluenceLayout.put("titleBGColor", "lightGrey");
        defaultConfluenceLayout.put("borderWidth", "1px");
    }
    
    @Mock
    private DescriptorFacade descriptorFacade;
    
    /**
     * Default constructor.
     */
    public ThemeDescriptorFacadeMock() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Setup mocking of built-in them loading.
     * @param registry The language registry to mock loading for
     */
    public void setupMock(final ThemeRegistryImpl registry) {
        registry.setDescriptorFacade(this.descriptorFacade);
        when(descriptorFacade.listBuiltinThemes()).thenReturn(BUILTIN_THEMES);
    }

}
